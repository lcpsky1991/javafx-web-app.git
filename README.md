# javafx-web-app

#### 介绍
javafx搭建的桌面app快速开发脚手架
- 集成WebView嵌入浏览器随钻客户端程序
- 主要技术为javaFX,sfnttool.jar(google字体精简工具库)

![image-20210315210454155](.\img\image-20210315210454155.png)
#### 安装教程

1. 安装java8
3. 命令行执行java -jar javafx-web-app.jar或
javaw -jar javafx-web-app.jar

#### 使用说明

1.  在文字输入区域数据要包含的字库，点击精简字库可以生成相应的简化ttf字库
2.  点击生成json可以生成json字库文件
3.  检查当前网络状态，连接到服务器自动更新程序
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

