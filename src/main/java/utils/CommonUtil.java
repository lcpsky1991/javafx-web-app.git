package utils;

import sample.Main;

import java.io.*;
import java.nio.charset.Charset;

/**
 * @author: Administrator
 * @date: 2021/05/11 9:36
 * @description:
 */
public class CommonUtil {

    public static String getStringFromInputStream(InputStream inputStream,Charset charset) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream,charset));
        StringBuffer buffer = new StringBuffer();
        String line = "";
        while ((line = in.readLine()) != null){
            buffer.append(line);
        }
        return buffer.toString();
    }

    public static String getFilePath(){
        String jarWholePath = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        try {
            jarWholePath = java.net.URLDecoder.decode(jarWholePath, "UTF-8");
        } catch (UnsupportedEncodingException e) { System.out.println(e.toString()); }
        String jarPath = new File(jarWholePath).getParentFile().getAbsolutePath();
        return  jarPath;
    }
}
