package server;


import cn.hutool.http.HttpUtil;
import cn.hutool.http.server.SimpleServer;
import cn.hutool.json.JSONUtil;
import sample.MainController;
import utils.CommonUtil;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;


public class LocalServer {


    public static void startLocalServer() throws Exception {
        SimpleServer server = HttpUtil.createServer(8082);
        server.addAction("/font/convert", (request, res) -> {
            FileInputStream fis = null;
            BufferedOutputStream bos = null;
            try {
                fis = new FileInputStream(MainController.fileDes);
                byte[] tmp = new byte[fis.available()];
                fis.read(tmp);
                res.setContentType("application/octet-stream");
                res.setHeader("Content-Disposition",
                        "attachment;fileName=" + new String("msyh_simplify.ttf".getBytes("utf-8"), "iso8859-1"));
                res.setHeader("Content-Length", String.valueOf(tmp.length));
                bos = new BufferedOutputStream(res.getOut());
                bos.write(tmp, 0, tmp.length);
                bos.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (fis!=null) {
                    try {
                        fis.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                if (bos!=null) {
                    try {
                        bos.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        //用来处理前端传来的数据
        server.addAction("/push/json", (request, res) -> {
          String output = (String) JSONUtil.parseArray(request.getParams().get("res")).get(0);
          String fileName = (String) JSONUtil.parseArray(request.getParams().get("name")).get(0);
            System.out.println(res+"----"+fileName);
          String filePath = MainController.getBasicFilePath()+ File.separator+fileName;
            Files.write(Paths.get(filePath),output.getBytes(Charset.defaultCharset()));
        });
        //用来处理前端传来的数据
        server.addAction("/version", (request, res) -> {
            OutputStream out = res.getOut();
            try {
                InputStream inputStream = LocalServer.class.getClassLoader().getResourceAsStream("version.json");
                String input = CommonUtil.getStringFromInputStream(inputStream,Charset.forName("UTF-8"));
                out.write(input.getBytes("GB2312"));
                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                if (out!=null) {
                    try {
                        out.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        server.start();
    }



}