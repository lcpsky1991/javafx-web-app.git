package sample;

import com.mohamnag.fxwebview_debugger.DevToolsDebuggerServer;
import elements.ConfirmWin;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ref.R;
import server.LocalServer;
import utils.RuntimeUtil;
import utils.UpdateUtil;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("sample.fxml"));
        primaryStage.setTitle(R.title);
        primaryStage.setScene(new Scene(root));
        Image icon = new Image(R.ICON, 32, 32, false, false);
        primaryStage.getIcons().add(icon);
        primaryStage.show();
        if (UpdateUtil.isOnline("www.baidu.com",1,150)){
            ConfirmWin confirmWin = new ConfirmWin();
            if(ConfirmWin.isOk){
                DevToolsDebuggerServer.stopDebugServer();
                RuntimeUtil.restartApp();
            }else {
                LocalServer.startLocalServer();
            }
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
