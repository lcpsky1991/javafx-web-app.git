package sample;

import elements.Navigatebar;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import ref.R;
import utils.FontMakeUtil;
import utils.WebViewUtil;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    private HBox menubar;
    @FXML
    private HBox contentArea;
    @FXML
    private AnchorPane MainView;
    @FXML
    private TextArea wordInputs;
    @FXML
    private Button btnDownload;
    @FXML
    private WebView centerWebView;
    private Navigatebar sysMenubar;

    public static String fileDes = getBasicFilePath()+File.separator+"msyh_simplify.ttf";


    public static WebEngine webEngine;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (menubar.getChildren().size() == 0) {
            sysMenubar = new Navigatebar();
            menubar.getChildren().add(sysMenubar.getSysMenubar());
            sysMenubar.getSysMenubar().prefWidthProperty().bind(MainView.widthProperty());
            wordInputs.prefWidthProperty().bind(contentArea.widthProperty());
            wordInputs.prefHeightProperty().bind(contentArea.heightProperty());
        }
        wordInputs.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                sysMenubar.words = newValue;
            }
        });
        btnDownload.setOnAction(event -> {
            openFileLocation(fileDes);
        });
        WebViewUtil webViewUtil = WebViewUtil.getInstance();
        webEngine = webViewUtil.initCenterWebView(centerWebView, R.menuviewUrl1);

    }
    /**
     * Opens the file with the System default file explorer.
     *
     * @param path the path
     */
    public static void openFileLocation(String path) {
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            try {
                Runtime.getRuntime().exec("explorer.exe /select," + path);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }
    public static void fontConvert(String words){
        String fileSrc = getBasicFilePath()+File.separator+"msyh.ttf";
        File file = new File(fileDes);
        if(file.exists()){
            file.delete();
        }
        FontMakeUtil.convertToSmallFontFile(words, fileSrc, fileDes);
    }

    public static String getBasicFilePath(){
        String jarWholePath = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        try {
            jarWholePath = java.net.URLDecoder.decode(jarWholePath, "UTF-8");
        } catch (UnsupportedEncodingException e) { System.out.println(e.toString()); }
        String jarPath = new File(jarWholePath).getParentFile().getAbsolutePath();
        return  jarPath;
    }


}
