package elements;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class ImageBtn extends HBox {
    private String name;
    private ImageView imageIcon;
    private String resUrl;
    public ImageBtn(String name){
        super();
        this.name = name;
        initUI();
    }
    private void initUI() {
        this.getStyleClass().add("imageBtn");
        this.getStylesheets().add("style.css");
    }

    public String getName(){
        return name;
    }

    public void setGraphic(ImageView imageIcon) {
        this.imageIcon = imageIcon;
        this.getChildren().addAll(imageIcon,new Label(name));
    }
    public ImageView getGraphic() {
        return this.imageIcon;
    }

    public String getResUrl() {
        return resUrl;
    }

    public void setResUrl(String resUrl) {
        this.resUrl = resUrl;
    }
}
