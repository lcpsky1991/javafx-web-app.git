package elements;

import javafx.geometry.Pos;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import ref.Path;
import ref.R;
import sample.MainController;

import java.util.ArrayList;
import java.util.List;

public class Navigatebar {
    private HBox bgPane;
    private List<ImageBtn> menuItems = new ArrayList<>();
    private VBox content;
    public String words;
    public Navigatebar() {
        initMenuPanel();
        initUI();
    }

    private void initUI() {
        content = new VBox();
        content.setAlignment(Pos.CENTER);
        content.getChildren().add(bgPane);

    }

    private void initMenuPanel() {
        // 按钮区域
        bgPane = new HBox();
        bgPane.getStyleClass().add("navBtn");
        bgPane.getStylesheets().add("style.css");

        ImageBtn mv1 = new ImageBtn(R.menuview1);
        mv1.setGraphic(R.getImageIcon(30,30,Path.PEN));
        mv1.setResUrl(R.menuview1Res);
        menuItems.add(mv1);

        ImageBtn mv2 = new ImageBtn(R.menuview2);
        mv2.setGraphic(R.getImageIcon(30,30,Path.RUBBER));
        mv2.setResUrl(R.menuview2Res);
        menuItems.add(mv2);

        DropShadow shadow = new DropShadow();
        shadow.setColor(Color.rgb(0,0,0,0.3));
        for (ImageBtn menuItem : menuItems) {
            menuItem.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
                menuItem.setEffect(shadow);
            });
            menuItem.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
                menuItem.setEffect(null);
            });
            menuItem.setOnMouseClicked(event -> {
                String contentStr = ((ImageBtn) event.getSource()).getName();
                System.out.println(contentStr);
                if(words!=null && !"".equals(words) && R.menuview1.equals(contentStr)){
                    MainController.fontConvert(words);
                }
                if(R.menuview2.equals(contentStr)){
                    System.out.println("convertTTFtoJson");
                    MainController.webEngine.executeScript("convertTTFtoJson()");
                }
            });
        }
        bgPane.getChildren().addAll(menuItems);
    }


    public VBox getSysMenubar() {
        return content;
    }
}
