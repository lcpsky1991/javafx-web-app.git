package elements;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * @author: Administrator
 * @date: 2021/05/11 16:57
 * @description:
 */
public class ConfirmWin {
    public static boolean isOk = false;
    public ConfirmWin(){
        // 创建一个确认对话框
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        // 设置对话框的头部文本
        alert.setHeaderText("提示");
        // 设置对话框的内容文本
        alert.setContentText("发现新版本，是否需要在线更新？");
        // 显示对话框，并等待按钮返回
        Optional<ButtonType> buttonType = alert.showAndWait();
        // 判断返回的按钮类型是确定还是取消，再据此分别进一步处理
        // 单击了取消按钮CANCEL_CLOSE
        // 单击了确定按钮OK_DONE
        if (buttonType.get().getButtonData().equals(ButtonBar.ButtonData.OK_DONE)) {
            isOk = true;
        } else {
            isOk = false;
        }
    }
}
