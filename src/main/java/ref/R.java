package ref;


import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class R {
    public static String title = "字体库精简工具";
    static String[] menuLists;
    static {
        menuLists = new String[]{"精简字库","生成JSON"};
    }
    public static final String ICON = "/images/icon.png";

    public static ImageView getImageIcon(int w, int h, String path) {
        ImageView icon = new ImageView(new Image(path, w, h, false, false));
        return icon;
    }

    public static String menuview1 = menuLists[0];
    public static String menuview2 = menuLists[1];

    public static String menuviewUrl1 = "menuview1.html";
    public static String menuviewUrl2 = "menuview2.html";

    public static String menuview1Res = "menuview1.fxml";
    public static String menuview2Res = "menuview2.fxml";
}
