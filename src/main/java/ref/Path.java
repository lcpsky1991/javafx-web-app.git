package ref;

public class Path {

    /**
     * 笔
     */
    public static final String PEN = "/toolbar/pen.png";
    /**
     * 橡皮
     */
    public static final String RUBBER = "/toolbar/rubber.png";
    /**
     * 涂漆
     */
    public static final String BARREL = "/toolbar/barrel.png";

}
