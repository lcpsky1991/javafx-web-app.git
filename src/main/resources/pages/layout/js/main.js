var restrictCharactersCheck = document.getElementById("restrictCharacters");
var restrictCharacterSetInput = document.getElementById("restrictCharacterSet");
window.onload = function() {
    restrictCharacterSetInput.disabled = !restrictCharactersCheck.checked;
}


function convertTTFtoJson() {
    opentype.load('http://127.0.0.1:8082/font/convert', function(err, font) {
        if (err)
            throw err;
        var result = convert(font);
        exportString(result, font.familyName + "_" + font.styleName + ".json");
    });


}
restrictCharactersCheck.onchange = function() {
    restrictCharacterSetInput.disabled = !restrictCharactersCheck.checked;
};
var exportString = function(output, filename) {

    var blob = new Blob([output], {
        type: 'text/plain'
    });
    var objectURL = URL.createObjectURL(blob);

    var link = document.createElement('a');
    link.href = objectURL;
    link.download = filename || 'data.json';
    link.target = '_blank';
    // link.click();

    var event = document.createEvent("MouseEvents");
    event.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false,
        false, false, false, 0, null);
    link.dispatchEvent(event);
    //将json传回cs端的服务器
    var res = {"res":output,"name":filename}
    $.ajax({
        url:'http://127.0.0.1:8082/push/json',
        type: 'POST',
        data: res,
        dataType: 'JSON',
        success: function (data) {
        }
    });
};

var convert = function(font) {

    console.log(font);

    var scale = (1000 * 100) / ((font.unitsPerEm || 2048) * 72);
    var result = {};
    result.glyphs = {};

    var restriction = {
        range: null,
        set: null
    };

    if (restrictCharactersCheck.checked) {
        var restrictContent = restrictCharacterSetInput.value;
        var rangeSeparator = '-';
        if (restrictContent.indexOf(rangeSeparator) != -1) {
            var rangeParts = restrictContent.split(rangeSeparator);
            if (rangeParts.length === 2 && !isNaN(rangeParts[0]) &&
                !isNaN(rangeParts[1])) {
                restriction.range = [parseInt(rangeParts[0]),
                    parseInt(rangeParts[1])
                ];
            }
        }
        if (restriction.range === null) {
            restriction.set = restrictContent;
        }
    }

    font.glyphs
        .forEach(function(glyph) {
            if (glyph.unicode !== undefined) {
                var glyphCharacter = String.fromCharCode(glyph.unicode);
                var needToExport = true;
                if (restriction.range !== null) {
                    needToExport = (glyph.unicode >= restriction.range[0] && glyph.unicode <= restriction.range[1]);
                } else if (restriction.set !== null) {
                    needToExport = (restrictCharacterSetInput.value
                        .indexOf(glyphCharacter) != -1);
                }
                if (needToExport) {

                    var token = {};
                    token.ha = Math.round(glyph.advanceWidth * scale);
                    token.x_min = Math.round(glyph.xMin * scale);
                    token.x_max = Math.round(glyph.xMax * scale);
                    token.o = ""

                    glyph.path.commands.forEach(function(command, i) {
                        if (command.type.toLowerCase() === "c") {
                            command.type = "b";
                        }
                        token.o += command.type.toLowerCase();
                        token.o += " "
                        if (command.x !== undefined &&
                            command.y !== undefined) {
                            token.o += Math.round(command.x * scale);
                            token.o += " "
                            token.o += Math.round(command.y * scale);
                            token.o += " "
                        }
                        if (command.x1 !== undefined &&
                            command.y1 !== undefined) {
                            token.o += Math.round(command.x1 * scale);
                            token.o += " "
                            token.o += Math.round(command.y1 * scale);
                            token.o += " "
                        }
                        if (command.x2 !== undefined &&
                            command.y2 !== undefined) {
                            token.o += Math.round(command.x2 * scale);
                            token.o += " "
                            token.o += Math.round(command.y2 * scale);
                            token.o += " "
                        }
                    });
                    result.glyphs[String.fromCharCode(glyph.unicode)] = token;
                }
            };
        });
    result.familyName = font.familyName;
    result.ascender = Math.round(font.ascender * scale);
    result.descender = Math.round(font.descender * scale);
    result.underlinePosition = Math.round(font.tables.post.underlinePosition *
        scale);
    result.underlineThickness = Math.round(font.tables.post.underlineThickness *
        scale);
    result.boundingBox = {
        "yMin": Math.round(font.tables.head.yMin * scale),
        "xMin": Math.round(font.tables.head.xMin * scale),
        "yMax": Math.round(font.tables.head.yMax * scale),
        "xMax": Math.round(font.tables.head.xMax * scale)
    };
    result.resolution = 1000;
    result.original_font_information = font.tables.name;
    if (font.styleName.toLowerCase().indexOf("bold") > -1) {
        result.cssFontWeight = "bold";
    } else {
        result.cssFontWeight = "normal";
    };

    if (font.styleName.toLowerCase().indexOf("italic") > -1) {
        result.cssFontStyle = "italic";
    } else {
        result.cssFontStyle = "normal";
    };
    return JSON.stringify(result);
};